#ifndef __VOCAB_TRAINING_APP__API__HPP
#define __VOCAB_TRAINING_APP__API__HPP

#include <string>

namespace app
{
class api
{
public:
    std::string version() const;
};
} // namespace app

#endif // __VOCAB_TRAINING_APP__API__HPP
