#ifndef __VOCAB_TRAINING_APP__CONNECTION_FACTORY__HPP
#define __VOCAB_TRAINING_APP__CONNECTION_FACTORY__HPP

#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <model/locale.hpp>
#include <model/meaning.hpp>
#include <model/word.hpp>
#include <sqlite_orm/sqlite_orm.h>
#include <string>

namespace app
{
class connection_factory
{
public:
    auto create_locale_table_definition() const
    {
        using namespace sqlite_orm;
        return make_table("locale", make_column("id", &app::model::locale::id, primary_key()),
                          make_column("key", &app::model::locale::key, unique()));
    }

    auto create_meaning_table_definition() const
    {
        using namespace sqlite_orm;
        using namespace app::model;

        return make_table("meaning", make_column("id", &meaning::id, primary_key()));
    }

    auto create_word_table_definition() const
    {
        using namespace sqlite_orm;
        using namespace app::model;

        return make_table("word", make_column("id", &word::id, primary_key()),
                          make_column("meaning_id", &word::meaning_id), make_column("locale_id", &word::locale_id),
                          make_column("value", &word::value), foreign_key(&word::meaning_id).references(&meaning::id),
                          foreign_key(&word::locale_id).references(&locale::id));
    }

    auto create(const std::string& filename) const
    {
        using namespace sqlite_orm;
        return make_storage(filename, create_locale_table_definition(), create_meaning_table_definition(),
                            create_word_table_definition());
    }

    std::string generate_key() const
    {
        using namespace boost::uuids;
        return to_string(random_generator()());
    }
};
} // namespace app

#endif // __VOCAB_TRAINING_APP__CONNECTION_FACTORY__HPP
