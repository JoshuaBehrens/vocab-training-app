#ifndef __VOCAB_TRAINING_APP__MODEL__LOCALE__HPP
#define __VOCAB_TRAINING_APP__MODEL__LOCALE__HPP

#include <string>

namespace app
{
namespace model
{
struct locale
{
    std::string id;
    std::string key;
};
} // namespace model
} // namespace app

#endif // __VOCAB_TRAINING_APP__MODEL__LOCALE__HPP
