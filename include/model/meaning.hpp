#ifndef __VOCAB_TRAINING_APP__MODEL__MEANING__HPP
#define __VOCAB_TRAINING_APP__MODEL__MEANING__HPP

#include <string>

namespace app
{
namespace model
{
struct meaning
{
    std::string id;
};
} // namespace model
} // namespace app

#endif // __VOCAB_TRAINING_APP__MODEL__MEANING__HPP
