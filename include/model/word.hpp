#ifndef __VOCAB_TRAINING_APP__MODEL__WORD__HPP
#define __VOCAB_TRAINING_APP__MODEL__WORD__HPP

#include <string>

namespace app
{
namespace model
{
struct word
{
    std::string id;
    std::unique_ptr<std::string> meaning_id;
    std::unique_ptr<std::string> locale_id;
    std::string value;
};
} // namespace model
} // namespace app

#endif // __VOCAB_TRAINING_APP__MODEL__WORD__HPP
