#ifndef __VOCAB_TRAINING_APP__SERVER__HPP
#define __VOCAB_TRAINING_APP__SERVER__HPP

#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/address.hpp>
#include <cstdint>
#include <thread>

namespace app
{
class server
{
public:
    server() = default;
    server(const server&) = delete;
    server(const server&&) = delete;
    server& operator=(const server&) = delete;
    server& operator=(const server&&) = delete;
    ~server() { stop(); }

    bool try_find_bind_able_port(const boost::asio::ip::address& address, const uint16_t& start, const uint16_t& end,
                                 uint16_t& port);

    void start(const boost::asio::ip::address& address, const uint16_t& port);

    void stop();

private:
    boost::asio::io_context _context{1};
    std::thread _run;
    bool _running = false;
};
} // namespace app

#endif // __VOCAB_TRAINING_APP__SERVER__HPP
