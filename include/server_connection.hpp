#ifndef __VOCAB_TRAINING_APP__SERVER_CONNECTION__HPP
#define __VOCAB_TRAINING_APP__SERVER_CONNECTION__HPP

#include <boost/asio.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <memory>

namespace app
{
class server_connection : public std::enable_shared_from_this<server_connection>
{
public:
    typedef boost::asio::ip::tcp::socket socket_t;

    explicit server_connection(socket_t socket) : _socket(std::move(socket)) {}

    void start();

private:
    typedef boost::beast::flat_buffer _buffer_t;
    typedef boost::beast::http::request<boost::beast::http::dynamic_body> _request_t;
    typedef boost::beast::http::response<boost::beast::http::dynamic_body> _response_t;
    typedef boost::asio::steady_timer _timer_t;

    socket_t _socket;
    _buffer_t _read_buffer_{8192};
    _request_t _request;
    _response_t _response;
    _timer_t _deadline{_socket.get_executor().context(), std::chrono::seconds(60)};

    void _read_request();
    void _process_request();
    void _write_response();
    void _check_deadline();
};
} // namespace app

#endif // __VOCAB_TRAINING_APP__SERVER_CONNECTION__HPP
