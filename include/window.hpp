#ifndef __VOCAB_TRAINING_APP__WINDOW__HPP
#define __VOCAB_TRAINING_APP__WINDOW__HPP

#include <api.hpp>
#include <string>
#include <webview.h>

namespace app
{
class window
{
public:
    typedef struct webview context_t;
    typedef context_t* context_ptr_t;

    window(const std::string& title, const std::string& url, const api& api);
    window(const window&) = delete;
    window& operator=(const window&) = delete;
    window(window&&) = delete;
    window& operator=(window&&) = delete;
    ~window();

    void run();

    context_ptr_t context();

    app::api& api();

private:
    context_t _context;
    app::api _api;
};
} // namespace app

#endif // __VOCAB_TRAINING_APP__WINDOW__HPP
