#include <connection_factory.hpp>
#include <cstdio>
#include <iostream>
#include <server.hpp>
#include <string>
#include <window.hpp>

std::string build_url(const boost::asio::ip::address& address, const uint16_t& port)
{
    auto address_string = address.to_string();
    char buffer[address_string.length() + 14];
    std::sprintf(buffer, "http://%s:%hu", address_string.c_str(), port);
    return {buffer};
}

int main()
{
    uint16_t port;
    auto const address = boost::asio::ip::make_address("0.0.0.0");

    if (!app::server().try_find_bind_able_port(address, 9000, 40000, port)) {
        std::cerr << "Error: Could not find a suitable port between 9000 and 40000" << std::endl;
        return 1;
    }

    auto connection = app::connection_factory().create("vocab_training.sqlite3");
    connection.sync_schema();

    app::api api;
    app::server server;
    server.start(address, port);

    app::window("Vocab", build_url(address, port), api).run();

    return 0;
}
