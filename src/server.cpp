#include "server.hpp"
#include <boost/asio/ip/tcp.hpp>
#include <functional>
#include <server_connection.hpp>

using app::server;

bool server::try_find_bind_able_port(const boost::asio::ip::address& address, const uint16_t& start,
                                     const uint16_t& end, uint16_t& port)
{
    for (port = start; port <= end; ++port) {
        try {
            boost::asio::ip::tcp::acceptor{_context, {address, port}};
        } catch (...) {
            continue;
        }

        return true;
    }

    return false;
}

void server::start(const boost::asio::ip::address& address, const uint16_t& port)
{
    stop();

    _run = std::thread([this, address, port]() {
        using namespace boost::asio::ip;
        using namespace boost;

        std::function<void(tcp::acceptor&, tcp::socket&)> loop;

        loop = [&loop](tcp::acceptor& acceptor, tcp::socket& socket) {
            acceptor.async_accept(socket, [&](beast::error_code ec) {
                if (!ec) {
                    std::make_shared<server_connection>(std::move(socket))->start();
                }

                loop(acceptor, socket);
            });
        };

        try {
            tcp::acceptor acceptor{this->_context, {address, port}};
            tcp::socket socket{this->_context};
            loop(acceptor, socket);
            this->_running = true;
            this->_context.run();
        } catch (std::exception const& e) {
        }
        this->_running = false;
    });
}

void server::stop()
{
    if (_running) {
        _context.stop();
        _run.join();
    }
}