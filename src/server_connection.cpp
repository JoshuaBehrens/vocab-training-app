#include "server_connection.hpp"
#include <resource_repository.hpp>

using app::server_connection;

void server_connection::start()
{
    _read_request();
    _check_deadline();
}

void server_connection::_read_request()
{
    auto self = shared_from_this();

    auto callback = [self](boost::beast::error_code ec, std::size_t bytes_transferred) {
        boost::ignore_unused(bytes_transferred);
        if (!ec) {
            self->_process_request();
        }
    };
    boost::beast::http::async_read(_socket, _read_buffer_, _request, callback);
}

void server_connection::_process_request()
{
    _response.version(_request.version());
    _response.keep_alive(false);

    if (_request.target() == "/index.html" || _request.target() == "/") {
        _response.set(boost::beast::http::field::content_type, "text/html");
        boost::beast::ostream(_response.body()) << web_app::resource_repository::files_index_html.string();
    } else if (_request.target() == "/js/api.js") {
        _response.set(boost::beast::http::field::content_type, "application/javascript");
        boost::beast::ostream(_response.body()) << web_app::resource_repository::files_js_api_js.string();
    } else if (_request.target() == "/js/binding.js") {
        _response.set(boost::beast::http::field::content_type, "application/javascript");
        boost::beast::ostream(_response.body()) << web_app::resource_repository::files_js_binding_js.string();
    } else {
        _response.result(boost::beast::http::status::not_found);
        _response.set(boost::beast::http::field::content_type, "text/plain");
        boost::beast::ostream(_response.body()) << "File not found\r\n";
    }

    _write_response();
}

void server_connection::_write_response()
{
    auto self = shared_from_this();

    _response.set(boost::beast::http::field::content_length, _response.body().size());

    boost::beast::http::async_write(_socket, _response, [self](boost::beast::error_code ec, std::size_t) {
        self->_socket.shutdown(boost::asio::ip::tcp::socket::shutdown_send, ec);
        self->_deadline.cancel();
    });
}

void server_connection::_check_deadline()
{
    auto self = shared_from_this();

    _deadline.async_wait([self](boost::beast::error_code ec) {
        if (!ec) {
            self->_socket.close(ec);
        }
    });
}
