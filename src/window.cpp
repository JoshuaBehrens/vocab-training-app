#include "window.hpp"
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <iostream>

using app::api;
using app::window;
using namespace boost::property_tree;

typedef struct
{
    std::string call, callback;
    ptree arguments;
} request_t;

request_t unpack_request(const char* body)
{
    std::istringstream data_stream(body);
    ptree result;
    read_json(data_stream, result);

    return {.call = result.get_child("call").get_value<std::string>(),
            .callback = result.get_child("callback").get_value<std::string>(),
            .arguments = result.get_child("args")};
}

void send_response(window* win, const request_t& request, const ptree& response)
{
    std::ostringstream result;
    ptree wrapper, callback_wrapper;
    result << "binding.callback(";
    callback_wrapper.put_value(request.callback);
    wrapper.add_child("callback", callback_wrapper);
    wrapper.add_child("result", response);
    write_json(result, wrapper);
    result << ")";
    webview_eval(win->context(), result.str().c_str());
}

void js_callback(struct webview* w, const char* arg)
{
    auto request = unpack_request(arg);
    auto win = (window*)w->userdata;
    auto api = win->api();
    ptree result;

    if (request.call == "getVersion") {
        result.put_value(api.version());
    }

    send_response(win, request, result);
}

window::window(const std::string& title, const std::string& url, const app::api& api)
    : _context({.title = title.c_str(),
                .url = url.c_str(),
                .width = 800,
                .height = 600,
                .debug = false,
                .resizable = true,
                .userdata = this,
                .external_invoke_cb = js_callback}),
      _api(api)
{
    webview_init(context());
}

window::~window()
{
    webview_exit(context());
    webview_terminate(context());
}

void window::run()
{
    while (webview_loop(context(), true) == 0)
        ;
}

window::context_ptr_t window::context()
{
    return &_context;
}

api& window::api()
{
    return _api;
}
