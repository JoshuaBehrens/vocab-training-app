init_api = function (binding) {
    const instance = {
        getVersion: function (back) {
            binding.invoke({
                call: 'getVersion',
                args: {}
            }, back);
        }
    };

    return instance;
};
