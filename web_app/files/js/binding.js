init_binding = function (window) {
    const callbacks = {};

    const makeId = function (length) {
        let result = '',
            characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
            charactersLength = characters.length;

        for (let i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }

        return result;
    };

    const makeUniqueId = function () {
        let result = '';

        do {
            result = makeId(24);
        } while (result in callbacks);

        return result;
    };

    const instance = {
        callback: function (param) {
            if (param.callback in callbacks) {
                callbacks[param.callback](param.result);
                delete callbacks[param.callback];
            }
        },

        add: function (callback) {
            const handle = makeUniqueId();
            callbacks[handle] = callback;
            return handle;
        },

        invoke: function (payload, back) {
            payload.callback = instance.add(back);
            window.external.invoke(JSON.stringify(payload));
        },
    };

    return instance;
};
