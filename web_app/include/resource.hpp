#ifndef __VOCAB_TRAINING_WEB_APP__RESOURCE__HPP
#define __VOCAB_TRAINING_WEB_APP__RESOURCE__HPP

#include <cstddef>
#include <string>

namespace web_app
{
class resource
{
public:
    resource(const char* start, const char* end) : _data(start), _size(end - start) {}

    const char* const& data() const { return _data; }
    std::string string() const { return {begin(), end()}; }
    const size_t& size() const { return _size; }
    const char* begin() const { return _data; }
    const char* end() const { return _data + _size; }

private:
    const char* _data;
    size_t _size;
};
} // namespace web_app

#endif // __VOCAB_TRAINING_WEB_APP__RESOURCE__HPP
