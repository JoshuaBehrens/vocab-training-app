#ifndef __VOCAB_TRAINING_WEB_APP__RESOURCE_REPOSITORY__HPP
#define __VOCAB_TRAINING_WEB_APP__RESOURCE_REPOSITORY__HPP

#include <resource.hpp>

namespace web_app
{
class resource_repository
{
public:
    static const resource files_index_html;
    static const resource files_js_api_js;
    static const resource files_js_binding_js;
};
} // namespace web_app

#endif // __VOCAB_TRAINING_WEB_APP__RESOURCE_REPOSITORY__HPP
